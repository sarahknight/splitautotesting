Feature: Testing Login Page
@regression
@login
Scenario Outline: Login with correct details
Given I am a registered tenant
When I log in with Email "<Email>" and Password "<Password>"
Then I should be logged in with my Name "<Name>" and Address "<Address>" displayed

Examples:
| Email         | Password  | Name         |Address |
| bobby@ar8.com | password1 | Bobby Rhodesa |APARTMENT 1, DOVE TREE COURT, 287, STRATFORD ROAD |