Feature: to test signup
@regression
@signup
Scenario Outline: to automate user signup process
Given I am viewing the splitbills signup page
When I enter Email "<Email>"
And Password "<Password>"
And Personal Details are "<Title>" "<First Name>" "<Last Name>" "<Phone Number>" "<Date of Birth>"
And Property Details are "<Postcode>" "<Address>" "<No of Tenants>" "<Landlord Name>" "<Days to Tenancy Start>" "<Days to Tenancy End>"
And Services Details are "<Services>" "<BB Package>" "<Sky Package>" "<Days to BBSky Start Date>"
And I get a valid quote for "<No of Tenants>" tenants at "<Address>" with "<Services>"
And Housemate Details are "<Housemate1 Name>" "<Housemate1 Email>" "<Housemate1 Mobile>"
And Direct Debit Details are "<First Name>" "<Last Name>" "<Sort Code>" "<Account Number>"
And Out of Term Address is "<Out of Term Postcode>" "<Out of Term Address>"
  Then SignUp is Successful
And I have a link to share with my housemates
  And there is a link to view the DD Mandate

  Examples: Happy Path
Email = if these are set to 'auto'  a unique address or email will be generated
  Postcode and Address options = actual postcode, nonvirgin, anywhereAnyBroadband
For tenancy start and end date, enter the number of days since or to the Tenancy started.  eg if it started yesterday enter -1, if it starts tomorrow enter 1
For Services, enter each service separated by a comma, eg Gas, Electric, Water, Broadband, TV-License, Sky-TV
| Email | Password  | Title | First Name | Last Name | Phone Number | Date of Birth | Postcode | Address   | No of Tenants | Landlord Name   | Days to Tenancy Start | Days to Tenancy End |Services| BB Package|Sky Package|Days to BBSky Start Date|Housemate1 Name|Housemate1 Email|Housemate1 Mobile| Sort Code | Account Number| Out of Term Postcode | Out of Term Address |
#| auto  | P@ssword.1 | Ms    | Al     | Smith  | 07777777777  | 01/01/1981    | nonvirgin | nonvirgin      | 2             | Ms Sally Smith  | 1                    | 327                 | water | Plusnet Broadband      |The Box Sets Bundle       |-6                      |Harry Housemate|auto            |07777777777      | 200000    | 55997711 |CV35 8NB | 4 GREAT PINLEY BARNS, PINLEY |
| auto  | P@ssword.1 | Ms    | Alice     | Smith  | auto  | 01/01/1981    | virgin | virgin      | 2             | Ms Sally Smith  | 1                      | 327                 | Gas, Electric, Water,TV-License,Broadband | Plusnet Broadband      |The Box Sets Bundle       |1                     |Harry Smith|auto           |auto      | 200000    | 55997711 |CV35 8NB | 4 GREAT PINLEY BARNS, PINLEY |

  @SwitchingBanks
  Scenario Outline: to automate user signup process
    Given I am viewing the splitbills signup page
    When I enter Email "<Email>"
    And Password "<Password>"
    And Personal Details are "<Title>" "<First Name>" "<Last Name>" "<Phone Number>" "<Date of Birth>"
    And Property Details are "<Postcode>" "<Address>" "<No of Tenants>" "<Landlord Name>" "<Days to Tenancy Start>" "<Days to Tenancy End>"
    And Services Details are "<Services>" "<BB Package>" "<Sky Package>" "<Days to BBSky Start Date>"
    And I get a valid quote for "<No of Tenants>" tenants at "<Address>"
    And Housemate Details are "<Housemate1 Name>" "<Housemate1 Email>" "<Housemate1 Mobile>"
    And Direct Debit Details are "<First Name>" "<Last Name>" "<Sort Code>" "<Account Number>"
    And Out of Term Address is "<Out of Term Postcode>" "<Out of Term Address>"
    Then SignUp is Successful
    And I have a link to share with my housemates
    And the page displays my "<First Name>" "<Last Name>" "<Phone Number>" "<Date of Birth>" "<Postcode>" "<Address>" "<No of Tenants>" "<Landlord Name>" "<Days to Tenancy Start>" "<Days to Tenancy End>"

    Examples: Happy Path
    Address & Email = if these are set to 'auto'  a unique address or email will be generated
    For tenancy start and end date, enter the number of days since or to the Tenancy started.  eg if it started yesterday enter -1, if it starts tomorrow enter 1
    For Services, enter each service separated by a comma, eg Gas, Electric, Water, Broadband, TV-License, Sky-TV
      | Email | Password  | Title | First Name | Last Name | Phone Number | Date of Birth | Postcode | Address   | No of Tenants | Landlord Name   | Days to Tenancy Start | Days to Tenancy End |Services| BB Package|Sky Package|Days to BBSky Start Date|Housemate1 Name|Housemate1 Email|Housemate1 Mobile| Sort Code | Account Number| Out of Term Postcode | Out of Term Address |
      #| auto  | P@ssword.1 | Ms    | Al     | Smith  | 07777777777  | 01/01/1981    | nonvirgin | nonvirgin      | 2             | Ms Sally Smith  | 1                    | 327                 | water | Plusnet Broadband      |The Box Sets Bundle       |-6                      |Harry Housemate|auto            |07777777777      | 200000    | 55997711 |CV35 8NB | 4 GREAT PINLEY BARNS, PINLEY |
      | auto  | P@ssword.1 | Ms    | Switching     | Smith  | 07777777777  | 01/01/1981    | nonvirgin | nonvirgin      | 2             | Ms Sally Smith  | 1                    | 327                 | water,broadband | Plusnet Broadband      |The Box Sets Bundle       |1                     |Switching Green|auto           |auto      | 200000    | 55997711 |CV35 8NB | 4 GREAT PINLEY BARNS, PINLEY |