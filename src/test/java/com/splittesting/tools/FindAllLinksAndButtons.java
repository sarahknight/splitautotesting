package com.splittesting.tools;

import com.splittesting.web.support.Driver;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import javax.print.DocFlavor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.splittesting.tools.checkBrokenLinks.testStatusCode;

/**
 * Finds all the links on a page
 * outputs their destination URL
 * Finds all the buttons on a page
 */
public class FindAllLinksAndButtons {

    public static void main(String[] args) {

        //WebDriver driver = new FirefoxDriver();
        String url1 = "https://www.splitthebills.co.uk/offers/offers-signup-1mfbb";
        String url2 = "https://uat.splitthebills.co.uk/offers/offers-signup-1mfbb";

        List<String> list1 = createListOfLinks(url1, "list1");
        List<String> list2 = createListOfLinks(url2, "list2");

        compareListSize(list1, list2);

        compareResources(list1, list2);

        compareEnvironments(list1, list2);

        checkSecondListForBrokenLinks(list2);


    }

    public static List createListOfLinks(String url, String arrayName) {

        WebDriver driver = new FirefoxDriver();
        driver = new FirefoxDriver();
        driver.get(url);

        java.util.List<WebElement> linkElements = driver.findElements(By.tagName("a"));
        java.util.List<String> linkTextAndUrls = new ArrayList<String>();

        System.out.println(linkElements.size());

        for (int i = 0; i < linkElements.size(); i = i + 1)

        {
            //System.out.println(i);
            //System.out.println(linkElements.get(i).getText());
            //System.out.println(linkElements.get(i).getAttribute("href"));
            linkTextAndUrls.add(i, linkElements.get(i).getText() + "," + linkElements.get(i).getAttribute("href"));
            System.out.println(linkTextAndUrls.get(i));


        }
        System.out.println("Number of buttons " + driver.findElements(By.tagName("Button")).size());
        driver.close();
        return linkTextAndUrls;
    }

    private static void compareListSize(List<String> list1, List<String> list2) {
        if (list1.size() != list2.size()) {
            System.out.println("DIFFERENT NUMBER OF LINKS ON PAGES");
        } else {
            System.out.println("Same number of links on pages");
        }
    }

    private static void compareResources(List<String> list1, List<String> list2) {
        //checks resource is the same
        for (int i = 0; i < list1.size(); i = i + 1) {
            if (list1.get(i).substring(list1.get(i).indexOf("k/") + 2).equals(list2.get(i).substring(list2.get(i).indexOf("k/") + 2))) {
                System.out.println("Different text for index: " + i + ". " + (list1.get(i).substring(list1.get(i).indexOf("k/") + 2)) + ".... " + (list2.get(i).substring(list2.get(i).indexOf("k/") + 2)));
            }
        }
    }

    private static void compareEnvironments(List<String> list1, List<String> list2) {
        for (int i = 0; i < list1.size(); i = i + 1) {
            int startEnv1 = list1.get(i).indexOf("//") + 2;
            int endEnv1 = startEnv1 + 3;

            int startEnv2 = list2.get(i).indexOf("//") + 2;
            int endEnv2 = startEnv2 + 3;

            System.out.println("ENVIRONMENTS: " + i + ". " + (list1.get(i).substring(startEnv1, endEnv1)) + ".... " + (list2.get(i).substring(startEnv2, endEnv2)));

        }
    }

    private static void checkSecondListForBrokenLinks(List<String> list) {
        for (int i = 0; i < list.size(); i = i + 1) {
            //extract second part of list
            String url = list.get(i).substring(list.get(i).lastIndexOf(",")+1);

            if (!url.equals("null")) {

                try {
                    testLinkStatusCode(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static void testLinkStatusCode(String restURL) throws ClientProtocolException, IOException {

        HttpUriRequest request = new HttpGet(restURL);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        //Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
        System.out.println(httpResponse.getStatusLine().getStatusCode());
    }

}