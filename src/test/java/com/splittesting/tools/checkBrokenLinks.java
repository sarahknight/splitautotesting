package com.splittesting.tools;


import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;

import java.io.IOException;

/**
 * Created by sarahknight on 11/07/2017.
 */
public class checkBrokenLinks {

    public static void main(String[] args) {

        try {
            testStatusCode("https://uat.splitthebills.co.uk/");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void testStatusCode(String restURL) throws ClientProtocolException, IOException {

        HttpUriRequest request = new HttpGet(restURL);
        HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        //Assert.assertEquals(httpResponse.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
        System.out.println(httpResponse.getStatusLine().getStatusCode());
    }





}
