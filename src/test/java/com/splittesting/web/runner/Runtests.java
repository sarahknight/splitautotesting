package com.splittesting.web.runner;

import com.splittesting.web.support.Browser;
import com.splittesting.web.support.Driver;
import com.splittesting.web.support.OsHelper;
import com.splittesting.web.support.TargetEnv;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import static io.github.seleniumquery.SeleniumQuery.$;
import static com.splittesting.web.support.OsHelper.getOs;


@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "@signup",
        features={"src/test/resources/com/splittesting/web/feature"}
        ,glue = {"com.splittesting.web.steps"},
        plugin = {"pretty", "html:build/reports/tests/uiTest"}
)

public class Runtests {
    private static Browser browser = Browser.CHROME; //SET BROWSER HERE
    private static String remoteWebDriverUrl = "http://localhost:4444/wd/hub";//CHANGE THIS WHEN WE KNOW WHAT IT IS
    private static TargetEnv targetEnvironment = TargetEnv.DEV; //choose: uat;rubydev; local

    @BeforeClass  //global hook
    public static void setup() {
        // Reconfigure driver for Mac. We can't use the remote driver with our
        // current Docker setup as the containers can't access the host
        // correctly due to `network_mode: host` not working in the
        // same way that it does on Linux.
        // See: https://github.com/docker/for-mac/issues/1031
        if (getOs().equals(OsHelper.MAC_OS)) {
            Driver.initiateLocal(browser, targetEnvironment);
        } else {
            // Reconfigure when running through CI
            if ("ci".equals(System.getProperty("spring.profiles.active"))) {
                remoteWebDriverUrl = "http://selenium-chrome:4444/wd/hub";
                targetEnvironment = TargetEnv.DEV;
            }

            Driver.initiateRemote(remoteWebDriverUrl, browser, targetEnvironment);
        }

        Driver.maximize();

        $.driver().use(Driver.webDriver);
    }

    @AfterClass  //global hook
    public static void tearDown() {
        Driver.close();
        Driver.quit();
    }
}