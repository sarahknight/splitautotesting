package com.splittesting.web.pageobjects;

import com.splittesting.web.support.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.splittesting.web.support.Utilities.*;

public class Signup_property_details_tenancy extends Driver {

    @FindBy(id = "numberOfTenants")
    private static WebElement txtNumberOfTenants;
    @FindBy(id = "landlordName")
    private static WebElement txtLandlordName;
    @FindBy(id = "submit")
    private static WebElement btnContinue;

    public void enterNumberOfTenants(String numberOfTenants, Boolean pressContinue) {
        waitForElementClickable(txtNumberOfTenants);
        //this JSE works at Selenium 2.52.  Upgrading may break it.
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", txtNumberOfTenants);
        txtNumberOfTenants.sendKeys(numberOfTenants);
        if(pressContinue) { pressContinueButton(); }
    }

    public void enterLandlordName(String landlordName, Boolean pressContinue) {
        waitForElementClickable(txtLandlordName);
        //this JSE works at Selenium 2.52.  Upgrading may break it.
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", txtLandlordName);
        txtLandlordName.sendKeys(landlordName);
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        //this JSE works at Selenium 2.52.  Upgrading may break it.
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", btnContinue);
        btnContinue.click();
    }
    /*
    public void enterAddress(String postcode, String fullstreetaddress, String houseNumberAndStreet) {
        minimiseChatWindow();
        String origPostcode = postcode; //has to remember if it's nonvirgin
        while (!(btnContinue.isEnabled())) {
            scrollToTop();
            postcodetextbox.clear();
            postcodetextbox.sendKeys("");

            if (origPostcode.equalsIgnoreCase("nonvirgin")){
                postcode = getNonVirginPostcode();
                //postcode = "TR3 6LT";
                System.out.println(postcode);
            }
            waitForElementClickable(postcodetextbox);
            postcodetextbox.sendKeys(postcode);
            waitForElementClickable(findaddressbutton);
            findaddressbutton.click();

            try {
                Thread.sleep(2000);
            }
            catch (Exception e){
                System.out.println(e);
            }
            waitForElementClickable(chooseaddressdropdown);
            chooseaddressdropdown.click();
            scrollPage(0, 330);

            //DROPDOWN HAS BEEN CLICKED; LIST IN VIEW

            //picks a random number within the size of the address list box and clicks on that in the list
            int numItemsInAddressList = fullstreetlistbox.size();//gets number of items in list
            Random num = new Random();
            int randomNumberInList = 0;
            do {
                randomNumberInList = num.nextInt(numItemsInAddressList);//creates random number in range of number of items in list
            }
            while(randomNumberInList==0);
            String searchString = fullstreetlistbox.get(randomNumberInList).getText(); //the address we are looking for

            //move mouse to be over first item in list
            //ON CHROME IT'S MOUSEDOWN+PAGEDOWN TO MOVE THROUGH THE BOX
            //scroll through list to find it (because we can't select things out of view)

            System.out.println("search string: " + searchString);
            System.out.println("index: " + fullstreetlistbox.indexOf((splitdriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath(".."))));
            System.out.println("item in list: " + fullstreetlistbox.get(fullstreetlistbox.indexOf((splitdriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))));
            System.out.println("y int: " + fullstreetlistbox.get(fullstreetlistbox.indexOf((splitdriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))).getLocation().getY());

            System.out.println(Math.round(FloatingDecimal.parseDouble(addresslistboxdiv.getCssValue("height").replace("px",""))));

            while(
                    (fullstreetlistbox.get(fullstreetlistbox.indexOf((splitdriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))).getLocation().getY()
                    ) >
                            (
                                    Math.round(FloatingDecimal.parseDouble(addresslistboxdiv.getCssValue("height").replace("px","")))+addresslistboxdiv.getLocation().getY()
                            )) {

                Actions action = new Actions(splitdriver);

                action.moveToElement(addresslistboxdiv,30,30).clickAndHold().sendKeys(Keys.ARROW_DOWN).release().build().perform();

                waitForElementClickable(chooseaddressdropdown);
                if(!(addresslistboxdiv.isDisplayed())) {
                    chooseaddressdropdown.click();
                }

            }
            fullstreetlistbox.get(fullstreetlistbox.indexOf((splitdriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))).click();
        }
    }


    public void enterNumberOfTenants(String numberOfTenants) {
        txtNumberOfTenants.sendKeys(numberOfTenants);
    }

    public void enterLandlordName(String landlordName){
        txtLandlordName.sendKeys(landlordName);
    }

    public void enterTenancyStartDate(long daysToTenancyStart) {
        scrollPage(0,450);
        tenancystartdatetextbox.click();
        datepickerSelectDaysFromToday(yeardatepickerheader,monthdatepickerheader,datepickernextbutton,datepickerpreviousbutton, daysToTenancyStart);

    }

    public void enterTenancyEndDate(long daysToTenancyEnd) {
        tenancyenddatetextbox.click();
        //scrollPage(0,400);
        datepickerSelectDaysFromToday(yeardatepickerheader,monthdatepickerheader,datepickernextbutton,datepickerpreviousbutton, daysToTenancyEnd);

    }

    public String getNonVirginPostcode() {
        String[] nonVirginPostcodes = {"TR1 1AA","TR1 1AF","TR1 1AG","TR1 1AH","TR1 1AJ","TR1 1AL","TR1 1AN","TR1 1AP","TR1 1AQ","TR1 1AR","TR1 1AT","TR1 1AU","TR1 1AW","TR1 1AX","TR1 1AY","TR1 1AZ","TR1 1BA","TR1 1BB","TR1 1BE","TR1 1BG","TR1 1BH","TR1 1BJ","TR1 1BL","TR1 1BN","TR1 1BP","TR1 1BQ","TR1 1BS","TR1 1BT","TR1 1BU","TR1 1BW","TR1 1BX","TR1 1BY","TR1 1BZ","TR1 1DA","TR1 1DB","TR1 1DD","TR1 1DE","TR1 1DF","TR1 1DG","TR1 1DH","TR1 1DL","TR1 1DN","TR1 1DP","TR1 1DQ","TR1 1DR","TR1 1DS","TR1 1DT","TR1 1DU","TR1 1DW","TR1 1DY","TR1 1EB","TR1 1ED","TR1 1EE","TR1 1EF","TR1 1EG","TR1 1EH","TR1 1EJ","TR1 1EL","TR1 1EN","TR1 1EP","TR1 1EQ","TR1 1ER","TR1 1ES","TR1 1ET","TR1 1EU","TR1 1EW","TR1 1EX","TR1 1EY","TR1 1EZ","TR1 1FA","TR1 1FF","TR1 1FG","TR1 1FN","TR1 1FP","TR1 1FX","TR1 1FY","TR1 1GB","TR1 1GE","TR1 1GH","TR1 1GP","TR1 1GR","TR1 1GS","TR1 1GT","TR1 1GW","TR1 1HA","TR1 1HB","TR1 1HD","TR1 1HE","TR1 1HF","TR1 1HG","TR1 1HH","TR1 1HJ","TR1 1HL","TR1 1HN","TR1 1HP","TR1 1HQ","TR1 1HR","TR1 1HS","TR1 1HT","TR1 1HU","TR1 1HW","TR1 1HX","TR1 1HY","TR1 1HZ","TR1 1JA","TR1 1JB","TR1 1JD","TR1 1JE","TR1 1JF","TR1 1JG","TR1 1JH","TR1 1JJ","TR1 1JL","TR1 1JN","TR1 1JP","TR1 1JR","TR1 1JS","TR1 1JT","TR1 1JU","TR1 1JW","TR1 1JX","TR1 1LA","TR1 1LD","TR1 1LE","TR1 1LF","TR1 1LG","TR1 1LH","TR1 1LJ","TR1 1LL","TR1 1LN","TR1 1LP","TR1 1LQ","TR1 1LR","TR1 1LS","TR1 1LT","TR1 1LU","TR1 1LW","TR1 1LX","TR1 1LY","TR1 1LZ","TR1 1NA","TR1 1ND","TR1 1NE","TR1 1NF","TR1 1NG","TR1 1NH","TR1 1NJ","TR1 1NL","TR1 1NN","TR1 1NP","TR1 1NQ","TR1 1NS","TR1 1NT","TR1 1NU","TR1 1NW","TR1 1NX","TR1 1NY","TR1 1NZ","TR1 1PA","TR1 1PB","TR1 1PD","TR1 1PE","TR1 1PF","TR1 1PG","TR1 1PH","TR1 1PJ","TR1 1PL","TR1 1PN","TR1 1PP","TR1 1PQ","TR1 1PR","TR1 1PS","TR1 1PT","TR1 1PU","TR1 1PW","TR1 1PX","TR1 1PZ","TR1 1QA","TR1 1QB","TR1 1QD","TR1 1QE","TR1 1QH","TR1 1QL","TR1 1QN","TR1 1QP","TR1 1QQ","TR1 1QR","TR1 1QS","TR1 1QT","TR1 1QW","TR1 1QX","TR1 1QY","TR1 1QZ","TR1 1RA","TR1 1RB","TR1 1RD","TR1 1RE","TR1 1RF","TR1 1RG","TR1 1RH","TR1 1RJ","TR1 1RL","TR1 1RN","TR1 1RP","TR1 1RQ","TR1 1RR","TR1 1RS","TR1 1RT","TR1 1RU","TR1 1RW","TR1 1RX","TR1 1RY","TR1 1RZ","TR1 1SA","TR1 1SB","TR1 1SD","TR1 1SE","TR1 1SF","TR1 1SG","TR1 1SH","TR1 1SJ","TR1 1SL","TR1 1SN","TR1 1SP","TR1 1SQ","TR1 1SR","TR1 1SS","TR1 1ST","TR1 1SU","TR1 1SW","TR1 1SX","TR1 1SY","TR1 1SZ","TR1 1TA","TR1 1TB","TR1 1TD","TR1 1TE","TR1 1TF","TR1 1TG","TR1 1TH","TR1 1TJ","TR1 1TL","TR1 1TN","TR1 1TP","TR1 1TQ","TR1 1TR","TR1 1TS","TR1 1TT","TR1 1TU","TR1 1TW","TR1 1TX","TR1 1TZ","TR1 1UA","TR1 1UB","TR1 1UD","TR1 1UE","TR1 1UF","TR1 1UH","TR1 1UJ","TR1 1UP","TR1 1UR","TR1 1UT","TR1 1UU","TR1 1UZ","TR1 1WE","TR1 1WL","TR1 1WT","TR1 1XE","TR1 1XF","TR1 1XJ","TR1 1XN","TR1 1XQ","TR1 1XR","TR1 1XT","TR1 1XU","TR1 1XW","TR1 1YB","TR1 1YE","TR1 1YL","TR1 1YP","TR1 1YR","TR1 1YS","TR1 1YT","TR1 1YZ","TR1 1ZJ","TR1 1ZQ","TR1 2AA","TR1 2AF","TR1 2AH","TR1 2AJ","TR1 2AP","TR1 2AQ","TR1 2AR","TR1 2AX","TR1 2AY","TR1 2AZ","TR1 2BB","TR1 2BD","TR1 2BE","TR1 2BG","TR1 2BH","TR1 2BJ","TR1 2BL","TR1 2BN","TR1 2BP","TR1 2BQ","TR1 2BS","TR1 2BT","TR1 2BU","TR1 2BW","TR1 2BX","TR1 2BY","TR1 2BZ","TR1 2DA","TR1 2DB","TR1 2DD","TR1 2DE","TR1 2DF","TR1 2DG","TR1 2DH","TR1 2DJ","TR1 2DL","TR1 2DN","TR1 2DP","TR1 2DQ","TR1 2DS","TR1 2DT","TR1 2DU","TR1 2DW","TR1 2DX","TR1 2DY","TR1 2DZ","TR1 2EA","TR1 2EB","TR1 2ED","TR1 2EE","TR1 2EF","TR1 2EG","TR1 2EH","TR1 2EJ","TR1 2EL","TR1 2EN","TR1 2EP","TR1 2EQ","TR1 2ER","TR1 2ES","TR1 2FA","TR1 2FB","TR1 2FD","TR1 2FE","TR1 2FG","TR1 2FH","TR1 2GA","TR1 2GB","TR1 2GD","TR1 2GE","TR1 2GF","TR1 2GG","TR1 2GH","TR1 2GJ","TR1 2GL","TR1 2GN","TR1 2GP","TR1 2GQ","TR1 2HA","TR1 2HB","TR1 2HD","TR1 2HE","TR1 2HH","TR1 2HJ","TR1 2HL","TR1 2HN","TR1 2HP","TR1 2HR","TR1 2HS","TR1 2HT","TR1 2HU","TR1 2HW","TR1 2HX","TR1 2JA","TR1 2JB","TR1 2JF","TR1 2JH","TR1 2JJ","TR1 2JL","TR1 2JN","TR1 2JP","TR1 2JQ","TR1 2JR","TR1 2JS","TR1 2JT","TR1 2JU","TR1 2JW","TR1 2JX","TR1 2JY","TR1 2JZ","TR1 2LA","TR1 2LB","TR1 2LD","TR1 2LE","TR1 2LF","TR1 2LH","TR1 2LL","TR1 2LQ","TR1 2LS","TR1 2LW","TR1 2NA","TR1 2NB","TR1 2NE","TR1 2NG","TR1 2NH","TR1 2NL","TR1 2NR","TR1 2NS","TR1 2NX","TR1 2NY","TR1 2PA","TR1 2PB","TR1 2PD","TR1 2PE","TR1 2PF","TR1 2PH","TR1 2PJ","TR1 2PN","TR1 2PQ","TR1 2PR","TR1 2PU","TR1 2PY","TR1 2PZ","TR1 2QA","TR1 2QB","TR1 2QD","TR1 2QE","TR1 2QH","TR1 2QQ","TR1 2QS","TR1 2QT","TR1 2QU","TR1 2QW","TR1 2QZ","TR1 2RA","TR1 2RB","TR1 2RF","TR1 2RG","TR1 2RL","TR1 2RN","TR1 2RP","TR1 2RQ","TR1 2RS","TR1 2RT","TR1 2RU","TR1 2RW","TR1 2RX","TR1 2SD","TR1 2SF","TR1 2SJ","TR1 2SN","TR1 2SQ","TR1 2SR","TR1 2SS","TR1 2ST","TR1 2SU","TR1 2SW","TR1 2TF","TR1 2TH","TR1 2TN","TR1 2TR","TR1 2TZ","TR1 2UD","TR1 2UE","TR1 2UF","TR1 2UL","TR1 2UU","TR1 2UX","TR1 2UY","TR1 2WT","TR1 2XH","TR1 2XJ","TR1 2XN","TR1 2XP","TR1 2XR","TR1 2XT","TR1 2XW","TR1 2XX","TR1 2YQ","TR1 2YY","TR1 3AA","TR1 3AB","TR1 3AD","TR1 3AE","TR1 3AF","TR1 3AG","TR1 3AJ","TR1 3AN","TR1 3AP","TR1 3AQ","TR1 3AR","TR1 3AS","TR1 3AT","TR1 3AU","TR1 3AY","TR1 3BA","TR1 3BD","TR1 3BE","TR1 3BH","TR1 3BJ","TR1 3BL","TR1 3BN","TR1 3BP","TR1 3BU","TR1 3BW","TR1 3BX","TR1 3BY","TR1 3BZ","TR1 3DA","TR1 3DB","TR1 3DD","TR1 3DE","TR1 3DF","TR1 3DG","TR1 3DJ","TR1 3DL","TR1 3DN","TR1 3DP","TR1 3DQ","TR1 3DR","TR1 3DT","TR1 3DU","TR1 3DW","TR1 3DX","TR1 3DY","TR1 3DZ","TR1 3EA","TR1 3EB","TR1 3ED","TR1 3EE","TR1 3EF","TR1 3EG","TR1 3EH","TR1 3EJ","TR1 3EL","TR1 3EN","TR1 3EP","TR1 3ER","TR1 3ES","TR1 3ET","TR1 3EU","TR1 3EW","TR1 3EX","TR1 3EY","TR1 3EZ","TR1 3FA","TR1 3FB","TR1 3FD","TR1 3FE","TR1 3FF","TR1 3FG","TR1 3FH","TR1 3FJ","TR1 3FL","TR1 3FN","TR1 3FP","TR1 3FQ","TR1 3FR","TR1 3FT","TR1 3GA","TR1 3GB","TR1 3GD","TR1 3GE","TR1 3GH","TR1 3GJ","TR1 3GL","TR1 3GT","TR1 3GZ","TR1 3HA","TR1 3HD","TR1 3HG","TR1 3HH","TR1 3HJ","TR1 3HL","TR1 3HN","TR1 3HP","TR1 3HR","TR1 3HS","TR1 3HT","TR1 3HU","TR1 3HW","TR1 3HX","TR1 3HY","TR1 3HZ","TR1 3JA","TR1 3JD","TR1 3JE","TR1 3JF","TR1 3JG","TR1 3JH","TR1 3JJ","TR1 3JL","TR1 3JP","TR1 3JQ","TR1 3JR","TR1 3JS","TR1 3JT","TR1 3JU","TR1 3JX","TR1 3JY","TR1 3JZ","TR1 3LA","TR1 3LD","TR1 3LE","TR1 3LF","TR1 3LG","TR1 3LJ","TR1 3LN","TR1 3LP","TR1 3LQ","TR1 3LS","TR1 3LT","TR1 3LU","TR1 3LX","TR1 3NA","TR1 3NB","TR1 3ND","TR1 3NE","TR1 3NF","TR1 3NG","TR1 3NH","TR1 3NJ","TR1 3NL","TR1 3NN","TR1 3NP","TR1 3NQ","TR1 3NR","TR1 3NS","TR1 3NT","TR1 3NU","TR1 3NW","TR1 3NX","TR1 3NY","TR1 3NZ","TR1 3PA","TR1 3PD","TR1 3PE","TR1 3PF","TR1 3PG","TR1 3PH","TR1 3PJ","TR1 3PL","TR1 3PN","TR1 3PP","TR1 3PQ","TR1 3PR","TR1 3PS","TR1 3PT","TR1 3PU","TR1 3PW","TR1 3PX","TR1 3PY","TR1 3PZ","TR1 3QA","TR1 3QB","TR1 3QD","TR1 3QE","TR1 3QF","TR1 3QH","TR1 3QJ","TR1 3QL","TR1 3QN","TR1 3QP","TR1 3QQ","TR1 3QR","TR1 3QS","TR1 3QT","TR1 3QU","TR1 3QW","TR1 3QX","TR1 3QY","TR1 3QZ","TR1 3RA","TR1 3RB","TR1 3RD","TR1 3RE","TR1 3RG","TR1 3RH","TR1 3RJ","TR1 3RL","TR1 3RN","TR1 3RP","TR1 3RQ","TR1 3RR","TR1 3RS","TR1 3RT","TR1 3RU","TR1 3RW","TR1 3RX","TR1 3RY","TR1 3RZ","TR1 3SA","TR1 3SB","TR1 3SD","TR1 3SE","TR1 3SF","TR1 3SG","TR1 3SH","TR1 3SJ","TR1 3SL","TR1 3SN","TR1 3SP","TR1 3SQ","TR1 3SR","TR1 3SS","TR1 3ST","TR1 3SU","TR1 3SW","TR1 3SX","TR1 3SY","TR1 3SZ","TR1 3TA","TR1 3TB","TR1 3TD","TR1 3TE","TR1 3TF","TR1 3TG","TR1 3TH","TR1 3TJ","TR1 3TL","TR1 3TN","TR1 3TP","TR1 3TQ","TR1 3TR","TR1 3TS","TR1 3TT","TR1 3TU","TR1 3TW","TR1 3TX","TR1 3TY","TR1 3TZ","TR1 3UA","TR1 3UB","TR1 3UD","TR1 3UE","TR1 3UF","TR1 3UL","TR1 3UN","TR1 3UP","TR1 3UR","TR1 3UT","TR1 3UU","TR1 3UY","TR1 3UZ","TR1 3WJ","TR1 3WL","TR1 3WN","TR1 3WR","TR1 3WW","TR1 3WZ","TR1 3XA","TR1 3XB","TR1 3XE","TR1 3XF","TR1 3XG","TR1 3XH","TR1 3XJ","TR1 3XL","TR1 3XN","TR1 3XQ","TR1 3XX","TR1 3YA","TR1 3YB","TR1 3YE","TR1 3YJ","TR1 3YL","TR1 9AG","TR1 9AZ","TR1 9BA","TR1 9BD","TR1 9BF","TR1 9BL","TR1 9DA","TR1 9DH","TR1 9DP","TR1 9EQ","TR1 9EX","TR1 9FB","TR1 9FH","TR1 9FN","TR1 9GE","TR1 9GH","TR1 9GP","TR1 9GR","TR1 9HA","TR1 9HE","TR1 9HF","TR1 9HJ","TR1 9HL","TR1 9HP","TR1 9HS","TR1 9HT","TR1 9HU","TR1 9HW","TR1 9HX","TR1 9HY","TR1 9HZ","TR3 6AA","TR3 6AB","TR3 6AD","TR3 6AE","TR3 6AF","TR3 6AG","TR3 6AH","TR3 6AJ","TR3 6AL","TR3 6AN","TR3 6AP","TR3 6AR","TR3 6AS","TR3 6AT","TR3 6AU","TR3 6AW","TR3 6AX","TR3 6AY","TR3 6BA","TR3 6BB","TR3 6BD","TR3 6BE","TR3 6BG","TR3 6BH","TR3 6BJ","TR3 6BL","TR3 6BN","TR3 6BP","TR3 6BQ","TR3 6BS","TR3 6BT","TR3 6BU","TR3 6BW","TR3 6BX","TR3 6BY","TR3 6BZ","TR3 6DA","TR3 6DB","TR3 6DD","TR3 6DE","TR3 6DF","TR3 6DG","TR3 6DH","TR3 6DJ","TR3 6DL","TR3 6DN","TR3 6DP","TR3 6DQ","TR3 6DR","TR3 6DS","TR3 6DT","TR3 6DU","TR3 6DW","TR3 6DX","TR3 6DY","TR3 6DZ","TR3 6EA","TR3 6EB","TR3 6ED","TR3 6EE","TR3 6EF","TR3 6EG","TR3 6EH","TR3 6EL","TR3 6EN","TR3 6EP","TR3 6EQ","TR3 6ER","TR3 6ES","TR3 6ET","TR3 6EU","TR3 6EW","TR3 6EX","TR3 6EY","TR3 6EZ","TR3 6FD","TR3 6FE","TR3 6FF","TR3 6FG","TR3 6FH","TR3 6FL","TR3 6FN","TR3 6FQ","TR3 6FR","TR3 6FS","TR3 6GA","TR3 6GB","TR3 6GD","TR3 6GE","TR3 6GG","TR3 6GH","TR3 6GJ","TR3 6GL","TR3 6GN","TR3 6GP","TR3 6GR","TR3 6GU","TR3 6HA","TR3 6HB","TR3 6HD","TR3 6HE","TR3 6HF","TR3 6HH","TR3 6HJ","TR3 6HL","TR3 6HN","TR3 6HP","TR3 6HQ","TR3 6HR","TR3 6HS","TR3 6HT","TR3 6HU","TR3 6HW","TR3 6HX","TR3 6HY","TR3 6HZ","TR3 6JA","TR3 6JB","TR3 6JD","TR3 6JE","TR3 6JF","TR3 6JG","TR3 6JH","TR3 6JJ","TR3 6JL","TR3 6JN","TR3 6JP","TR3 6JQ","TR3 6JR","TR3 6JS","TR3 6JT","TR3 6JU","TR3 6JW","TR3 6JX","TR3 6JY","TR3 6JZ","TR3 6LA","TR3 6LD","TR3 6LE","TR3 6LF","TR3 6LG","TR3 6LH","TR3 6LJ","TR3 6LL","TR3 6LN","TR3 6LP","TR3 6LQ","TR3 6LR","TR3 6LS","TR3 6LT","TR3 6LU","TR3 6LW","TR3 6LX","TR3 6LY","TR3 6LZ","TR3 6NA","TR3 6NB","TR3 6ND","TR3 6NE","TR3 6NF","TR3 6NG","TR3 6NH","TR3 6NJ","TR3 6NL","TR3 6NN","TR3 6NP","TR3 6NQ","TR3 6NR","TR3 6NS","TR3 6NT","TR3 6NU","TR3 6NW","TR3 6NX","TR3 6NY","TR3 6NZ","TR3 6PA","TR3 6PB","TR3 6PD","TR3 6PE","TR3 6PF","TR3 6PG","TR3 6PH","TR3 6PJ","TR3 6PL","TR3 6PN","TR3 6PP","TR3 6PQ","TR3 6PR","TR3 6PS","TR3 6PT","TR3 6PU","TR3 6PW","TR3 6PX","TR3 6PY","TR3 6PZ","TR3 6QA","TR3 6QB","TR3 6QE","TR3 6QG","TR3 6QH","TR3 6QJ","TR3 6QL","TR3 6QN","TR3 6QP","TR3 6QQ","TR3 6QR","TR3 6QS","TR3 6QU","TR3 6QW","TR3 6QX","TR3 6QY","TR3 6QZ","TR3 6RA","TR3 6RB","TR3 6RD","TR3 6RE","TR3 6RF","TR3 6RG","TR3 6RH","TR3 6RJ","TR3 6RL","TR3 6RN","TR3 6RP","TR3 6RQ","TR3 6RR","TR3 6RS","TR3 6RT","TR3 6RU","TR3 6RW","TR3 6RX","TR3 6SA","TR3 6SB","TR3 6SD","TR3 6SE","TR3 6SF","TR3 6SG","TR3 6SH","TR3 6SP","TR3 6SQ","TR3 6SR","TR3 6SS","TR3 6TP","TR3 6TR","TR3 6TS","TR3 6TT","TR3 6TU","TR3 6TX","TR3 6TZ","TR3 6UA","TR3 6UB","TR3 6WD","TR3 6XB","TR3 7AA","TR3 7AB","TR3 7AD","TR3 7AE","TR3 7AF","TR3 7AG","TR3 7AH","TR3 7AJ","TR3 7AL","TR3 7AN","TR3 7AP","TR3 7AQ","TR3 7AR","TR3 7AS","TR3 7AT","TR3 7AU","TR3 7AW","TR3 7AX","TR3 7AY","TR3 7AZ","TR3 7BA","TR3 7BB","TR3 7BD","TR3 7BE","TR3 7BG","TR3 7BH","TR3 7BJ","TR3 7BL","TR3 7BN","TR3 7BP","TR3 7BQ","TR3 7BR","TR3 7BS","TR3 7BT","TR3 7BU","TR3 7BW","TR3 7BX","TR3 7BY","TR3 7BZ","TR3 7DA","TR3 7DB","TR3 7DD","TR3 7DE","TR3 7DF","TR3 7DG","TR3 7DH","TR3 7DJ","TR3 7DL","TR3 7DN","TR3 7DP","TR3 7DQ","TR3 7DR","TR3 7DS","TR3 7DT","TR3 7DU","TR3 7DW","TR3 7DX","TR3 7DY","TR3 7DZ","TR3 7EA","TR3 7EB","TR3 7ED","TR3 7EE","TR3 7EF","TR3 7EH","TR3 7EJ","TR3 7EL","TR3 7EN","TR3 7EP","TR3 7ER","TR3 7ES","TR3 7ET","TR3 7EU","TR3 7EW","TR3 7EX","TR3 7EY","TR3 7EZ","TR3 7FA","TR3 7FB","TR3 7FD","TR3 7FE","TR3 7FF","TR3 7FG","TR3 7GA","TR3 7GD","TR3 7HA","TR3 7HB","TR3 7HD","TR3 7HE","TR3 7HF","TR3 7HG","TR3 7HH","TR3 7HJ","TR3 7HL","TR3 7HN","TR3 7HP","TR3 7HQ","TR3 7HR","TR3 7HS","TR3 7HT","TR3 7HU","TR3 7HW","TR3 7HY","TR3 7HZ","TR3 7JA","TR3 7JB","TR3 7JD","TR3 7JE","TR3 7JF","TR3 7JG","TR3 7JH","TR3 7JJ","TR3 7JL","TR3 7JN","TR3 7JP","TR3 7JQ","TR3 7JR","TR3 7JS","TR3 7JT","TR3 7JU","TR3 7JW","TR3 7JX","TR3 7JY","TR3 7JZ","TR3 7LA","TR3 7LB","TR3 7LD","TR3 7LE","TR3 7LF","TR3 7LG","TR3 7LH","TR3 7LJ","TR3 7LL","TR3 7LN","TR3 7LP","TR3 7LQ","TR3 7LR","TR3 7LS","TR3 7LT","TR3 7LU","TR3 7LW","TR3 7LX","TR3 7LY","TR3 7LZ","TR3 7NA","TR3 7NB","TR3 7ND","TR3 7NF","TR3 7NG","TR3 7NH","TR3 7NJ","TR3 7NL","TR3 7NN","TR3 7NP","TR3 7NQ","TR3 7NR","TR3 7NS","TR3 7NT","TR3 7NU","TR3 7NW","TR3 7NX","TR3 7NY","TR3 7NZ","TR3 7PA","TR3 7PB","TR3 7PD","TR3 7PE","TR3 7PF","TR3 7PG","TR3 7PH","TR3 7PJ","TR3 7PL","TR3 7PN","TR3 7PP","TR3 7PQ","TR3 7PR","TR3 7PS","TR3 7PT","TR3 7PU","TR3 7PW","TR3 7PX","TR3 7PY","TR3 7PZ","TR3 7QA","TR3 7QB","TR3 7QD","TR3 7QE","TR3 7QF","TR3 7QG","TR3 7QH","TR3 7QL","TR3 7QN","TR3 7QP","TR3 7QQ","TR3 7QR","TR3 7QS","TR3 7QT","TR3 7QU","TR3 7QW","TR3 7QX","TR3 7QY","TR3 7RA","TR3 7RB","TR3 7RD","TR3 7RE","TR3 7RF","TR3 7RG","TR3 7RH","TR3 7RJ","TR3 7RL","TR3 7RN","TR3 7RQ","TR3 7RS","TR3 7RT","TR3 7RW","TR3 7RX","TR3 7XZ","TR3 7ZQ"};
        String nonVirginPostcode = nonVirginPostcodes[ThreadLocalRandom.current().nextInt(0,nonVirginPostcodes.length)];
        return nonVirginPostcode;
    }
*/
}
