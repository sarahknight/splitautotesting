package com.splittesting.web.pageobjects;

import com.splittesting.web.support.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.splittesting.web.support.Utilities.*;

public class Signup_select_services extends Driver {
        @FindBy(css ="span.gas")
        private static WebElement gastoggle;
        @FindBy(css ="span.electric")
        private static WebElement electrictoggle;
        @FindBy(css ="span.water")
        private static WebElement watertoggle;
        @FindBy(css ="span.broadband")
        private static WebElement broadbandtoggle;
        @FindBy(css ="span.tv-licence")
        private static WebElement tvlicencetoggle;
        @FindBy(css ="span.sky-tv")
        private static WebElement skytvtoggle;
        @FindBy(id = "submit")
        private static WebElement btnContinue;
        @FindBy(css = "div#broadband-package a.icon-chevron-down")
        private static WebElement broadbanddropdown;
        @FindBy(css = "div#broadband-package div.options-wrap li")
        private static List<WebElement> broadbandPackageList;
        @FindBy(css = "div#service-start-date span.icon-chevron-down")
        private static WebElement broadbandskystartdatedropdown;
        @FindBy(css = "span.ui-datepicker-year")
        private static WebElement yeardatepickerheader;
        @FindBy(css = "span.ui-datepicker-month")
        private static WebElement monthdatepickerheader;
        @FindBy(css = "span.ui-icon-circle-triangle-e")
        private static WebElement datepickernextbutton;
        @FindBy(css = "span.ui-icon-circle-triangle-w")
        private static WebElement datepickerpreviousbutton;
        @FindBy(css = "div#sky-tv-package a.icon-chevron-down")
        private static WebElement skypackagedropdown;
        @FindBy(css = "div#sky-tv-package div.options-wrap li")
        private static List<WebElement> skyPackageList;
        @FindBy(css = "div#sky-tv-package li")
        private static List<WebElement> skypackagelistbox;
        @FindBy(css="div#sky-tv-package ol")
        private static WebElement skypackagelistboxdiv;



    public void enterDetails(String services,String bbPackage,String skyPackage,long daysToBBSkyStartDate, Boolean pressContinue) {
        waitForElementClickable(gastoggle);
        selectServices(services);

        if((services.toLowerCase().contains("broadband"))||(services.toLowerCase().contains("sky"))) {
            scrollPage(0,500);
            waitForElementClickable(broadbandskystartdatedropdown);
            broadbandskystartdatedropdown.click();
            datepickerSelectDaysFromToday(yeardatepickerheader,monthdatepickerheader, datepickernextbutton, datepickerpreviousbutton, daysToBBSkyStartDate);
        }

        if (services.toLowerCase().contains("broadband")) {
            scrollToTop();
            scrollPage(0,500);
            selectBroadband(bbPackage, daysToBBSkyStartDate);
        }
        // TODO: 18/09/2016 Add method to select Virgin and Sky Packages


        if (services.toLowerCase().contains("sky-tv")) {
            scrollToTop();
            scrollPage(0,650);
            selectSkyPackage(skyPackage);
        }

        //clickContinue();
        if(pressContinue) { pressContinueButton(); }
    }

    public void selectSkyPackage(String skyPackage){
        waitForElementClickable(skypackagedropdown);
        skypackagedropdown.click();
        //clickItemInSinglePageList(broadbandPackageList, skyPackage);
        clickItemInMultiPageList(skypackagelistbox, skypackagelistboxdiv, skypackagedropdown, skyPackage);
    }


    public void selectBroadband(String bbPackage,long days){
        waitForElementClickable(broadbanddropdown);
        broadbanddropdown.click();
        clickItemInSinglePageList(broadbandPackageList, bbPackage);
    }


    public void selectServices(String services){
        services = services.replace(" ","");
        String[] allServices = services.split(",");
        for (String service : allServices){
            switch (service.toLowerCase()) {
                case "gas" :
                    gastoggle.click();
                    break;
                case "electric" :
                    electrictoggle.click();
                    break;
                case "water" :
                    watertoggle.click();
                    break;
                case "broadband" :
                    broadbandtoggle.click();
                    break;
                case "tv-license" :
                    tvlicencetoggle.click();
                    break;
                //case "sky-tv" :
                //    skytvtoggle.click();
                //    break;
            }

        }

    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }
}
