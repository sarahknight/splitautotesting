package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.splittesting.web.support.Utilities.*;

public class Signup_additional_details extends Driver {
        @FindBy(name = "postcode")
        private static WebElement postcodetextbox;
        @FindBy(css ="div.postcode > button.button")
        private static WebElement findaddressbutton;
        @FindBy(css = "div#address_list_section a.selection")
        private static WebElement chooseaddressdropdown;
        @FindBy(css = "div#address_list_section li")
        private static List<WebElement> fullstreetlistbox;
        @FindBy(id = "submit")
        private static WebElement btnContinue;


    public void enterDetails(String outOfTermPostcode, String outOfTermAddress, Boolean pressContinue) {
        waitForPageToLoad("additional_details");
        waitForElement(postcodetextbox);
        postcodetextbox.sendKeys(outOfTermPostcode);
        waitForElementClickable(findaddressbutton);
        findaddressbutton.click();
        waitForPageToLoad("additional_details");
        waitForElementClickable(chooseaddressdropdown);
        chooseaddressdropdown.click();
        scrollPage(0, 250);
        clickItemInSinglePageList(fullstreetlistbox, outOfTermAddress);
        if (pressContinue) { pressContinueButton();}

    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }

}


