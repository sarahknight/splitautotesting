package com.splittesting.web.pageobjects;


import static org.junit.Assert.*;

import com.splittesting.web.support.Driver;
import org.openqa.selenium.By;

public class Portal_student_main extends Driver {

    //@FindBy(id = "email")
    //private static WebElement txtEmailAddress;


    public void checkAddressDisplayed(String expectedAddress){
        String addressText = webDriver.findElement(By.xpath(("//main/div/div/div/h1"))).getText();
        assertTrue("The address displayed is incorrect.  Expected: " + expectedAddress + ". Name displayed: " + addressText, addressText.equals(expectedAddress));
        //TODO take screenshot here if assertion is false
    }

}