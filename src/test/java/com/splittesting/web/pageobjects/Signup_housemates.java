package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Instant;

import static com.splittesting.web.support.Utilities.*;

public class Signup_housemates extends Driver {
        @FindBy(css ="span.icon-chevron-down")
        private static WebElement housematedropdown;
        @FindBy(name = "name")
        private static WebElement txtHousemateName;
        @FindBy(name = "email")
        private static WebElement housemateemailtextbox;
        @FindBy(css="div.ajax-loading")
        private static WebElement loadingicon;
        @FindBy(name = "mobile")
        private static WebElement housematemobiletextbox;
        @FindBy(id = "submit")
        private static WebElement btnContinue;

    public void enterDetails(String housemateName, String housemateEmail, String housemateMobile,Boolean pressContinue) {
        if(housemateEmail.toLowerCase().equals("auto")) { //to create a unique email address each time
            housemateEmail = Long.toString(Instant.now().getEpochSecond()) + "@example.com";
        }
        if(housemateMobile.toLowerCase().equals("auto")) {
            housemateMobile="07" + generateRandomNumber(9);
        }

        waitForPageToLoad("housemates");
        waitForElementClickable(housematedropdown);
        housematedropdown.click();

        Point place = txtHousemateName.getLocation();
        //this JSE works at Selenium 2.53.  Upgrading may break it.
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollTo(" + place.getX() + "," + place.getY()+");", btnContinue);
        txtHousemateName.sendKeys(housemateName);
        waitForElementClickable(housemateemailtextbox);
        housemateemailtextbox.sendKeys(housemateEmail);
        //System.out.println("Housemate email:" + housemateEmail );
        waitForElementClickable(housematemobiletextbox);
        housematemobiletextbox.sendKeys(housemateMobile);
        //wait for validation of mobile field.  Tab is necessary to ensure field has been validated
        WebDriverWait wait = new WebDriverWait(webDriver, 40);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.ng-valid")));
        housematemobiletextbox.sendKeys(Keys.TAB);
        housematemobiletextbox.sendKeys(Keys.ARROW_DOWN);
        waitForElementClickable(btnContinue);
        scrollPage(0,250);

        if (pressContinue) { pressContinueButton(); }
    }
    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }
}
