package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static com.splittesting.web.support.Utilities.*;

public class Signup_personal_details extends Driver {

    @FindBy(name = "first_name")
    private static WebElement firstnametextbox;
    @FindBy(css = "a.icon-chevron-down")
    private static WebElement titledropdown;
    @FindBy(css = "div.options-wrap li")
    private static List<WebElement> titleList;
    @FindBy(name = "last_name")
    private static WebElement lastnametextbox;
    @FindBy(name = "mobilePhone")
    private static WebElement phonenumbertextbox;
    @FindBy(name = "dob")
    private static WebElement dateofbirthtextbox;
    @FindBy(id = "submit")
    private static WebElement btnContinue;

    public void enterAllDetails(String title, String firstName, String lastName, String phoneNumber, String dateOfBirth, Boolean pressContinue)
    {
        enterTitle(title);
        enterFirstName(firstName);
        enterLastName(lastName);
        enterPhone(phoneNumber);
        enterDateOfBirth(dateOfBirth);
        if(pressContinue) { pressContinueButton(); }
    }

    public void enterTitle(String title){
        waitForElementClickable(titledropdown);
        titledropdown.click();
        scrollPage(0, 250);
        clickItemInSinglePageList(titleList,title);
    }

    public void enterFirstName(String firstName) {
        firstnametextbox.sendKeys(firstName);
    }

    public void enterLastName(String lastName) {
        lastnametextbox.sendKeys(lastName);
    }

    public void enterPhone(String phone) {

        if (phone.equals("auto")) {
            phone = "07" + generateRandomNumber(9);
        }
        phonenumbertextbox.sendKeys(phone);
    }

    public void enterDateOfBirth(String dateOfBirth) {
        dateofbirthtextbox.sendKeys(dateOfBirth);
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }
}
