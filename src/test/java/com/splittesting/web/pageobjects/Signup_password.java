package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import com.splittesting.web.support.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import static com.splittesting.web.support.Utilities.*;

public class Signup_password extends Driver {

    @FindBy(name = "password")
    private static WebElement txtPassword;
    @FindBy(name = "passwordConfirm")
    private static WebElement txtPasswordConfirm;
    @FindBy(id = "submit")
    private static WebElement btnContinue;

    public void enterPasswordAndConfirm(String password, Boolean pressContinue)
    {
        waitForElementClickable(txtPassword);
        txtPassword.sendKeys(password);
        txtPasswordConfirm.sendKeys(password);
        if (pressContinue) { pressContinueButton();}
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }
}
