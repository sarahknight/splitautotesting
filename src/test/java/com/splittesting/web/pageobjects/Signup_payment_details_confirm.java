package com.splittesting.web.pageobjects;

import com.splittesting.web.support.Driver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import static com.splittesting.web.support.Utilities.*;

public class Signup_payment_details_confirm extends Driver {
        @FindBy(id = "confirm")
        private static WebElement btnContinue;
        @FindBy(css ="body.ng-scope.top")
        private static WebElement topOfPage;
        @FindBy(css ="footer >div.copyright")
        private static WebElement bottomOfPage;



    public void pressContinueButton(Boolean pressContinue) {
        waitForPageToLoad("");
        scrollToContinueButtonAndPress(btnContinue);
    }

}
