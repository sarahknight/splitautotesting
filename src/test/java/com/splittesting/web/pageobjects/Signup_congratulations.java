package com.splittesting.web.pageobjects;

import com.splittesting.web.support.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.splittesting.web.support.Utilities.scrollToContinueButtonAndPress;
import static com.splittesting.web.support.Utilities.waitForPageToLoad;

public class Signup_congratulations extends Driver {
        @FindBy(css ="a.join-link")
        private static WebElement lnkJoin;
        @FindBy(linkText = "here")
        private static WebElement lnkMandatePDF;

    public void checkSignUpSuccessText() {
        waitForPageToLoad("congratulations");
        String congratsText = webDriver.findElement(By.xpath(("//*[@id='angularRoot']/div/div/div/h1"))).getText();
        Assert.assertTrue("Signup was unsuccessful",congratsText.equals("Woohoo! You're all signed up."));
    }

    public void checkHousemateLink() {
        waitForPageToLoad("congratulations");
        String joinLink = lnkJoin.getAttribute("href");
        //System.out.println("joinLink= :" + joinLink);
        Assert.assertTrue("Join up link invalid",joinLink.matches("http:\\/\\/splt.uk\\/[a-zA-Z0-9]+"));

    }

    public void checkMandateLink() {
        waitForPageToLoad("congratulations");
        String mandateLink = lnkMandatePDF.getAttribute("href");
        //System.out.println("mandate url:= " + mandateLink);
        Assert.assertTrue("Mandate PDF Link is wrong", mandateLink.matches("https:\\/\\/mandate-previews-sandbox.gocardless.com\\/\\?token=[a-zA-Z0-9\\._-]+"));
    }
}
