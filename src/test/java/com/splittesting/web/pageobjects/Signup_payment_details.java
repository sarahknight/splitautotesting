package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.splittesting.web.support.Utilities.*;

public class Signup_payment_details extends Driver {
        @FindBy(name = "name")
        private static WebElement fullnametextbox;
        @FindBy(name = "sort_code")
        private static WebElement sortCodeTextBox;
        @FindBy(css="div.ajax-loading")
        private static WebElement loadingicon;
        @FindBy(name = "number")
        private static WebElement accountNumberTextBox;
        @FindBy(id = "submit")
        private static WebElement btnContinue;
        @FindBy(id = "agree")
        private static WebElement termsagreebox;
        @FindBy(name = "first_name")
        private static WebElement gcFirstNameTextBox;
        @FindBy(name = "last_name")
        private static WebElement gcLastNameTextBox;
        @FindBy(id="agree_acc_holder")
        private static WebElement chkAgreeAccountHolder;


    public void enterDetails(String firstName, String lastName, String sortCode, String accountNumber, Boolean pressContinue) {
        waitForPageToLoad("payment_details");
        scrollPage(0,250);
        waitForElementClickable(accountNumberTextBox);

        if (!webDriver.findElements(By.name("name")).isEmpty()) {//this is a goCardless payment page
            fullnametextbox.sendKeys(firstName + " " + lastName);
        }
        else{//not a goCardless payment page
            gcFirstNameTextBox.sendKeys(firstName);
            gcLastNameTextBox.sendKeys(lastName);
            scrollPage(0,800);
            chkAgreeAccountHolder.click();
        }
        sortCodeTextBox.sendKeys(sortCode);
        accountNumberTextBox.sendKeys(accountNumber);
        accountNumberTextBox.sendKeys(Keys.TAB);
        termsagreebox.click();
        if (pressContinue) { pressContinueButton();}
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);
    }
}
