package com.splittesting.web.pageobjects;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static com.splittesting.web.support.Utilities.*;
import static com.splittesting.web.support.Driver.webDriver;
import static com.splittesting.web.support.Strings.getTargetEnvironment;

public class Signup_email {

    @FindBy(name = "email")
    private static WebElement txtEmailAddress;
    @FindBy(id = "submit")
    private static WebElement btnContinue;

    public void gotosignuppage()
    {
        webDriver.get(getTargetEnvironment() + "/signup");
    }

    public void enterEmailAddress(String emailAddress,Boolean pressContinue)
    {
        waitForElementClickable(txtEmailAddress);
        if(emailAddress.toLowerCase().equals("auto")){ //autogenerate email address
            emailAddress = (generateRandomAlphaNumeric(10)+ "@example.com").toLowerCase();
        }
        txtEmailAddress.clear();
        txtEmailAddress.sendKeys("");//clears box so it can be reused in next test
        txtEmailAddress.sendKeys(emailAddress);
        System.out.println("EMAIL: " + emailAddress);
        if (pressContinue) { pressContinueButton();}
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        btnContinue.click();
        //scrollToContinueButtonAndPress(btnContinue);
    }
}
