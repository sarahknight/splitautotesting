package com.splittesting.web.pageobjects;


import com.splittesting.web.support.Driver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.splittesting.web.support.Utilities.*;

public class Signup_property_details_dates extends Driver {

    @FindBy(id = "select_tenancy_start")
    private static WebElement tenancystartdatetextbox;
    @FindBy(id = "select_tenancy_end")
    private static WebElement tenancyenddatetextbox;
    @FindBy(css = "span.ui-datepicker-year")
    private static WebElement yeardatepickerheader;
    @FindBy(css = "span.ui-icon-circle-triangle-e")
    private static WebElement datepickernextbutton;
    @FindBy(css = "span.ui-icon-circle-triangle-w")
    private static WebElement datepickerpreviousbutton;
    @FindBy(css = "div.icon_font.close")
    private static WebElement livechaticononlyclosebutton;
    @FindBy(css = "span.ui-datepicker-month")
    private static WebElement monthdatepickerheader;
    @FindBy(id = "submit")
    private static WebElement btnContinue;
    @FindBy(css = "div.ajax-loading")
    private static WebElement loadingicon;
    @FindBy(css = "div.overlay.jx_ui_Widget")
    private static WebElement zopimminimisebutton;
    @FindBy(css = "div.pseudo-select ol")
    private static WebElement addresslistboxdiv;

    public void enterTenancyStartDate(long daysToTenancyStart, Boolean pressContinue) {
        scrollPage(0, 450);
        tenancystartdatetextbox.click();
        datepickerSelectDaysFromToday(yeardatepickerheader, monthdatepickerheader, datepickernextbutton, datepickerpreviousbutton, daysToTenancyStart);
        if (pressContinue) { pressContinueButton(); }
    }

    public void enterTenancyEndDate(long daysToTenancyEnd, Boolean pressContinue) {
        tenancyenddatetextbox.click();
        //scrollPage(0,400);
        datepickerSelectDaysFromToday(yeardatepickerheader, monthdatepickerheader, datepickernextbutton, datepickerpreviousbutton, daysToTenancyEnd);
        if (pressContinue) { pressContinueButton(); }
    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        //this JSE works at Selenium 2.52.  Upgrading may break it.
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", btnContinue);
        btnContinue.click();
    }
}