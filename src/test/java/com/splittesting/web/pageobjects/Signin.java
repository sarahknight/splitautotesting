package com.splittesting.web.pageobjects;

import static com.splittesting.web.support.Driver.webDriver;
import static com.splittesting.web.support.Strings.getTargetEnvironment;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Signin {

    @FindBy(id = "email")
    private static WebElement txtEmailAddress;
    @FindBy(id = "password")
    private static WebElement txtPassword;
    @FindBy(css = "input.button")
    private static WebElement btnLogin;

    public void goToLoginPage()
    {

        webDriver.get(getTargetEnvironment());
 /*
        //go to correct page, depending on test environment
        switch (getTestEnvironment()) {
            case "rubydev":
                webDriver.get("https://ryan.splitthebills.co.uk/signin");
                break;
            case "uat":
                webDriver.get("https://uat.splitthebills.co.uk/signin");
                break;
            default:
                System.out.println("Unrecognised test environment: " + getTestEnvironment());

        }
        */
    }

    public void enterEmail(String emailAddress){
        txtEmailAddress.sendKeys(emailAddress);
    }

    public void enterPassword(String password){
        txtPassword.sendKeys(password);
    }

    public void clickLogin(){
        btnLogin.click();
    }
}