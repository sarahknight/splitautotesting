package com.splittesting.web.pageobjects;

import com.splittesting.web.support.Driver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.splittesting.web.pageobjects.Signup_property_details_address;


import static com.splittesting.web.pageobjects.Signup_property_details_address.propertyAddress;
import static com.splittesting.web.support.Utilities.scrollToContinueButtonAndPress;
import static com.splittesting.web.support.Utilities.waitForElementClickable;
import static com.splittesting.web.support.Utilities.waitForPageToLoad;

public class Signup_view_quote extends Driver {
        @FindBy(css ="div.title p.ng-binding")
        private static WebElement numberofstudentsstring;
        @FindBy(id = "save-quote")
        private static WebElement btnContinue;
        @FindBy(css="div.ajax-loading")
        private static WebElement loadingicon;


    public void checkDetails(String numberTenants, String address, String services, Boolean pressContinue) {
        //assertNumberOfStudents(numberTenants, address);
        waitForPageToLoad("view_quote");
        assertNumberOfStudents(numberTenants);
        assertPropertyAddress(propertyAddress);
        // TODO: 15/09/2016 check correct services are selected
        // TODO: 15/09/2016 check services quote amounts add up
        waitForElementClickable(btnContinue);
        waitForPageToLoad("view_quote");
        if(pressContinue){pressContinueButton();}


    }

    public void pressContinueButton() {
        waitForElementClickable(btnContinue);
        scrollToContinueButtonAndPress(btnContinue);

        /* old method
        waitForElementClickable(btnContinue);
        //this JSE works at Selenium 2.52.  Upgrading may break it.
        ((JavascriptExecutor) splitdriver).executeScript("arguments[0].scrollIntoView(true);", btnContinue);
        btnContinue.click();*/
    }

    public void assertNumberOfStudents(String numberTenants) {
        waitForElementClickable(btnContinue);
        Assert.assertTrue("Incorrect number of students on view-quote page",numberofstudentsstring.getText().contains("Based on "+numberTenants+" students"));
    }

    public void assertPropertyAddress(String propertyAddress){
        waitForElementClickable(btnContinue);
        Assert.assertTrue("Incorrect address on view-quote page",numberofstudentsstring.getText().contains("living at: " + propertyAddress));

    }

}
