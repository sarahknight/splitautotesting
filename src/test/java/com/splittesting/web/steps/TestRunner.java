package com.splittesting.web.steps;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;  //cucumber uses JUnit to execute the tests


//the RunWith(Cucumber.class) annotation
//overrides the default runner and tells JUnit to use Cucumber runner from Cucumber.class
//This means the TestRunner class has been instrumented in such a way that it will
// search for feature files and run them, providing the output back to JUnit in a format
// that it understands. You can control how Cucumber runs by annotating the test class with various options.
@RunWith(Cucumber.class)
//Next this is setting Cucumber so that it will be display a “pretty” format, which basically means
// console output in color, as well as an HTML report that will be stored
// in the target/cucumber-html-report directory.
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-html-report"},
        features="src/test/resources/com/splittesting"
)
public class TestRunner {

}
