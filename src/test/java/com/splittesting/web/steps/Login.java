package com.splittesting.web.steps;

import com.splittesting.web.support.Driver;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.support.PageFactory;
import com.splittesting.web.pageobjects.*;


public class Login extends Driver {
    public Signin loginPage;
    public Portal_student_navigationbar portalNavBar;
    public Portal_student_main portalMain;

    @Before
    public void before(){
        //System.out.println("oh ho ho");
    }

    @Rule
    public final TestName name = new TestName();



    @Given("^I am a registered tenant$")
    public void i_am_a_registered_tenant() throws Throwable {
        // nothing here yet, maybe link to customer database one day
    }

    @When("^I log in with Email \"([^\"]*)\" and Password \"([^\"]*)\"$")
    public void i_log_in_with_and(String arg1, String arg2) throws Throwable {
        loginPage = PageFactory.initElements(webDriver,Signin.class);
        loginPage.goToLoginPage();
        loginPage.enterEmail(arg1);
        loginPage.enterPassword(arg2);
        loginPage.clickLogin();
    }

    @Then("^I should be logged in with my Name \"([^\"]*)\" and Address \"([^\"]*)\" displayed$")
    public void iShouldBeLoggedInWithMyAndDisplayed(String arg0, String arg1) throws Throwable {
        portalNavBar = PageFactory.initElements(webDriver,Portal_student_navigationbar.class);
        portalNavBar.checkNameDisplayed(arg0);
        //portalMain = PageFactory.initElements(splitdriver,Portal_student_main.class);
        //portalMain.checkAddressDisplayed(arg1);

        //TODO checkHorizontalTabsDisplayed();
    }

    @After
    public void printScenarioStatus(Scenario scenario){
        System.out.println(scenario.getStatus());
        //System.out.println("tra la la");
        System.out.println(name.getMethodName());

    }
    //@After
    //public void printName(TestName name){
     //   System.out.println(name.getMethodName());
   // }
}
