package com.splittesting.web.steps;


import com.splittesting.web.support.Driver;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import com.splittesting.web.pageobjects.*;


public class Signup extends Driver {
    private Signup_email semailpage;
    private Signup_password spasswordpage;
    private Signup_personal_details spersonaldetails;
    private Signup_property_details_address spropertydetailsaddress;
    private Signup_property_details_tenancy spropertydetailstenancy;
    private Signup_property_details_dates spropertydetailsdates;
    private Signup_select_services ssignupselectservices;
    private Signup_view_quote ssignupviewquote;
    private Signup_housemates ssignuphousemates;
    private Signup_payment_details ssignuppaymentdetails;
    private Signup_payment_details_confirm ssignuppaymentdetailsconfirm;
    private Signup_additional_details ssignupadditionaldetails;
    private Signup_congratulations ssignupcongratulations;

    @Given("^I am viewing the splitbills signup page$")
    public void i_am_viewing_the_splitbills_signup_page() throws Throwable {
        semailpage = PageFactory.initElements(webDriver, Signup_email.class);
        semailpage.gotosignuppage();
    }

    @When("^I enter Email \"([^\"]*)\"$")
    public void i_enter_Email(String arg1) throws Throwable {
        semailpage.enterEmailAddress(arg1,true);
    }

    @When("^Password \"([^\"]*)\"$")
    public void password(String arg1) throws Throwable {
        spasswordpage=PageFactory.initElements(webDriver,Signup_password.class);
        spasswordpage.enterPasswordAndConfirm(arg1,true);
    }

    @When("^Personal Details are \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void personal_Details_are(String arg1, String arg2, String arg3, String arg4, String arg5) throws Throwable {
        spersonaldetails = PageFactory.initElements(webDriver,Signup_personal_details.class);
        spersonaldetails.enterAllDetails(arg1,arg2,arg3,arg4,arg5,true);
    }

    @When("^Property Details are \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void property_Details_are(String arg1, String arg2, String arg3, String arg4, long arg5, long arg6) throws Throwable {
        spropertydetailstenancy = PageFactory.initElements(webDriver,Signup_property_details_tenancy.class);
        spropertydetailstenancy.enterNumberOfTenants(arg3, false);
        spropertydetailstenancy.enterLandlordName(arg4, false);
        spropertydetailsdates = PageFactory.initElements(webDriver, Signup_property_details_dates.class);
        spropertydetailsdates.enterTenancyStartDate(arg5, false);
        spropertydetailsdates.enterTenancyEndDate(arg6,false);
        spropertydetailsaddress = PageFactory.initElements(webDriver,Signup_property_details_address.class);
        spropertydetailsaddress.enterAddress(arg1, true);
    }

    @When("^Services Details are \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void services_Details_are(String arg1, String arg2, String arg3, long arg4) throws Throwable {
        ssignupselectservices = PageFactory.initElements(webDriver, Signup_select_services.class);
        ssignupselectservices.enterDetails(arg1,arg2,arg3,arg4,true);
    }

    @And("^I get a valid quote for \"([^\"]*)\" tenants at \"([^\"]*)\" with \"([^\"]*)\"$")
    public void iGetAValidQuoteForTenantsAtWith(String arg1, String arg2, String arg3) throws Throwable {
        ssignupviewquote = PageFactory.initElements(webDriver,Signup_view_quote.class);
        ssignupviewquote.checkDetails(arg1, arg2, arg3, true);
    }

    @When("^Housemate Details are \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void housemate_Details_are(String arg1, String arg2, String arg3) throws Throwable {
        ssignuphousemates = PageFactory.initElements(webDriver,Signup_housemates.class);
        ssignuphousemates.enterDetails(arg1, arg2, arg3, true);
    }

    @When("^Direct Debit Details are \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void direct_Debit_Details_are(String arg1, String arg2, String arg3, String arg4) throws Throwable {
        ssignuppaymentdetails = PageFactory.initElements(webDriver,Signup_payment_details.class);
        ssignuppaymentdetails.enterDetails(arg1,arg2,arg3,arg4,true);
        ssignuppaymentdetailsconfirm = PageFactory.initElements(webDriver,Signup_payment_details_confirm.class);
        ssignuppaymentdetailsconfirm.pressContinueButton(true);

    }

    @When("^Out of Term Address is \"([^\"]*)\" \"([^\"]*)\"$")
    public void out_of_Term_Address_is(String arg1, String arg2) throws Throwable {
        ssignupadditionaldetails = PageFactory.initElements(webDriver, Signup_additional_details.class);
        ssignupadditionaldetails.enterDetails(arg1, arg2, true  );
    }

    @Then("^SignUp is Successful$")
    public void signup_is_Successful() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        ssignupcongratulations = PageFactory.initElements(webDriver,Signup_congratulations.class);
        ssignupcongratulations.checkSignUpSuccessText();
    }

    @Then("^I have a link to share with my housemates$")
    public void i_have_a_link_to_share_with_my_housemates() throws Throwable {
        ssignupcongratulations = PageFactory.initElements(webDriver,Signup_congratulations.class);
        ssignupcongratulations.checkHousemateLink();
    }


    @And("^there is a link to view the DD Mandate$")
    public void thereIsALinkToViewTheDDMandate() throws Throwable {
        ssignupcongratulations.checkMandateLink();
    }

}
