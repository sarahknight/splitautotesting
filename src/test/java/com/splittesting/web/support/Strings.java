package com.splittesting.web.support;


public class Strings {

    private static String testPath;

    public static String getTargetEnvironment() {
        return Driver.getTargetEnvironment().getUrl();
    }

    public static String getTestPath(String whichPath) {
        if (whichPath.equalsIgnoreCase("allCustomers")) {
            testPath = "/api/customers";
        }
        return testPath;
    }

    public static int getPropertyProgressBarTimeout() {
        return 8000;
    }
}
