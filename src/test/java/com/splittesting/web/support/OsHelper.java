package com.splittesting.web.support;

public class OsHelper {

    public static final String MAC_OS = "mac";
    public static final String LINUX_OS = "linux";
    public static final String WINDOWS_OS = "windows";
    public static final String OTHER_OS = "other";

    public static String getOs() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return WINDOWS_OS;
        } else if (os.contains("nux") || os.contains("nix")) {
            return LINUX_OS;
        } else if (os.contains("mac")) {
            return MAC_OS;
        } else {
            return OTHER_OS;
        }
    }
}
