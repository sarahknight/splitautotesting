package com.splittesting.web.support;

/**
 * The environment that is to be targeted by the browser.
 */
public enum TargetEnv {

    LOCAL("http://localhost:3000"),
    DEV("https://dev.splitthebills.co.uk"),
    QA("https://uat.splitthebills.co.uk");

    private String url;

    TargetEnv(final String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}