package com.splittesting.web.support;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import static com.splittesting.web.support.Browser.CHROME;
import static com.splittesting.web.support.Browser.FIREFOX;
import static com.splittesting.web.support.OsHelper.getOs;

public class Driver {

    public static WebDriver webDriver;
    public static TargetEnv targetEnvironment;

    /**
     * Use a remote driver. This should be used if intending to perform tests
     * on a consistent environment e.g. a Docker container with Selenium.
     * <p>
     * This is the preferred way to run the UI tests.
     *
     * @param webDriverUrl of the Selenium hub to run tests though.
     * @param browser      to run tests in.
     * @param targetEnv    to run the tests against.
     */
    public static void initiateRemote(String webDriverUrl, Browser browser, TargetEnv targetEnv) {
        webDriver = getRemoteDriver(webDriverUrl, browser);
        targetEnvironment = targetEnv;
    }

    /**
     * Use a local driver. This is here for cases where it might be
     * desirable to perform some testing directly through your own
     * browser.
     * <p>
     * The {@link this#initiateRemote} method should be preferred.
     *
     * @param browser   to run tests in.
     * @param targetEnv to run the tests against.
     */
    public static void initiateLocal(Browser browser, TargetEnv targetEnv) {
        webDriver = getLocalDriver(browser);
        targetEnvironment = targetEnv;
    }

    private static WebDriver getRemoteDriver(String remoteUrl, Browser browser) {
        Capabilities capabilities;

        switch (browser) {
            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;
            case CHROME:
                capabilities = DesiredCapabilities.chrome();
                break;
            default:
                throw new IllegalArgumentException("Unrecognised browser selected.");
        }

        try {
            return new RemoteWebDriver(new URL(remoteUrl), capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    private static WebDriver getLocalDriver(Browser browser) {
        switch (getOs()) {
            case OsHelper.MAC_OS:
                if (browser == FIREFOX) {
                    System.setProperty("webdriver.gecko.driver", "src/test/resources/vendor/geckodriver-mac64-0.18.0");
                    return new FirefoxDriver();
                }

                if (browser == CHROME)
{
                    System.setProperty("webdriver.chrome.driver", "src/test/resources/vendor/chromedriver-mac64-2.33");
                    return new ChromeDriver();
                }
                break;
            case OsHelper.LINUX_OS:
                if (browser == FIREFOX) {
                    System.setProperty(
                            "webdriver.gecko.driver",
                            "src/test/resources/vendor/geckodriver-linux64-0.18.0"
                    );
                    return new FirefoxDriver();
                }

                if (browser == CHROME) {
                    System.setProperty(
                            "webdriver.chrome.driver",
                            "src/test/resources/vendor/chromedriver-linux64-2.32"
                    );
                    return new ChromeDriver();
                }
            default:
                throw new IllegalArgumentException("Unrecognised browser selected.");
        }

        return null;
    }

    public static TargetEnv getTargetEnvironment() {
        return targetEnvironment;
    }

    public static void maximize() {
        webDriver.manage().window().setSize(new Dimension(1280, 1024));
    }

    public static void close() {
        webDriver.close();
    }

    public static void quit() {
        webDriver.quit();
    }
}
