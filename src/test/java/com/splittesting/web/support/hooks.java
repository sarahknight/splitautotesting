package com.splittesting.web.support;



import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;


public class hooks {

    @Before()
    public void beforeCallingScenario() {
        System.out.println("*********** About to start the scenario.");
    }

    public void beforeScenario(){
        //splitdriver.maximize();
    }

    @After
    public void afterRunningScenario(Scenario scenario) {
        System.out.println("*********** Just finished running scenario: "
                + scenario.getStatus());
    }
    public void takeScreenshot(Scenario scenario){
        if (scenario.isFailed()) {
            File scrFile = ((TakesScreenshot) Driver.webDriver).getScreenshotAs(OutputType.FILE); try {
                FileUtils.copyFile(scrFile, new File("failshot_"
                        + "test"
                        + ".png"));
            } catch (IOException exception) { exception.printStackTrace();
            }
        }}
    /*public void afterScenario(Scenario scenario){
        if (scenario.isFailed()) {
            WebDriver augmentedDriver = new Augmenter().augment(splitdriver);
            byte[] screenshot = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/jpeg");
        }*/


    }

