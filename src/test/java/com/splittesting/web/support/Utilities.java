package com.splittesting.web.support;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.misc.FloatingDecimal;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Random;

import static com.splittesting.web.support.Driver.webDriver;


public final class Utilities {

    @FindBy(css="div.overlay.jx_ui_Widget")
    private static WebElement zopimminimisebutton;

    public static void waitForElement(WebElement element)
    {
        WebDriverWait wait =new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public static void waitForElementClickable(WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    //To generate Random AlphaNumeric String
    public static String generateRandomAlphaNumeric(int length){
        return RandomStringUtils.randomAlphanumeric(length);
    }

    //To generate Random String
    public static String generateRandomString(int length){
        return RandomStringUtils.randomAlphabetic(length);
    }

    //To generate Random Numeric String
    public static String generateRandomNumber(int length){
        return RandomStringUtils.randomNumeric(length);
    }

    //scrolls left/right and up/down through page based on values in parameters
    public static void scrollPage(int horizontal, int vertical) {
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("scroll(" + horizontal + "," + vertical + ")");
    }

    public static void scrollToTop() {
        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("window.scrollTo(0,0)");

    }

    //finds and clicks on an item in list
    //Parameters: currentList = the List Element, searchString = the string to be clicked on
    public static void clickItemInSinglePageList(List<WebElement> currentList, String searchString) {
        for (int thisItem = 0; thisItem < currentList.size(); thisItem++) {
            if (currentList.get(thisItem).getText().equalsIgnoreCase(searchString)) {
                currentList.get(thisItem).click();
                return; //TODO needs code to handle string not being found
            }
        }
    }

    public static void datepickerSelectDaysFromToday(WebElement yeardatepickerheader,WebElement monthdatepickerheader, WebElement datepickernextbutton, WebElement datepickerpreviousbutton,  long days) {
        //this takes the 'days' parameter, adds (or subtracts) it from today's date, and then picks that date in the datepicker
        //so if I want to set a start date of 10 days ago, I use days = -10
        LocalDate dateToPick = LocalDate.now().plusDays(days);

        int findMonth = dateToPick.getMonth().getValue(); //the month as an int
        int findYear = dateToPick.getYear();
        String findDay = Integer.toString(dateToPick.getDayOfMonth());

        //read the year from the datepicker and compare with required value
        waitForElement(yeardatepickerheader);
        int datepickerYear = Integer.parseInt(yeardatepickerheader.getText());
        //now click 'previous' or 'next' to get to the required year
        if (datepickerYear != findYear) {
            while (datepickerYear < findYear) {
                datepickernextbutton.click();
                datepickerYear = Integer.parseInt(yeardatepickerheader.getText());
            }
            while (datepickerYear > findYear) {
                datepickerpreviousbutton.click();
                datepickerYear = Integer.parseInt(yeardatepickerheader.getText());
            }
        }
        //now click to the right month
        int datepickerMonth = Month.valueOf(monthdatepickerheader.getText().toUpperCase()).getValue();
        //now click 'previous' or 'next' arrows to get to the required month
        if (datepickerMonth != findMonth) {
            while (datepickerMonth < findMonth) {
                datepickernextbutton.click();
                datepickerMonth = Month.valueOf(monthdatepickerheader.getText().toUpperCase()).getValue();
            }
            while (datepickerMonth > findMonth) {
                datepickerpreviousbutton.click();
                datepickerMonth = Month.valueOf(monthdatepickerheader.getText().toUpperCase()).getValue();
            }
        }
        //finally click the correct day of the month
        //has to use xpath locate to find the actual day, as it's text and not in css
        //and there may be two elements with the same day, as the 30th will be displayed twice (previous month and this month)
        List<WebElement> lstStartDays = webDriver.findElements(By.xpath("//a[text() = '" + findDay + "']"));
        //and then iterate through the List to find the element that's not greyed out and click it
        for (WebElement temp : lstStartDays) {
            String currentElementClass = temp.getAttribute("class");
            if (currentElementClass.contains("secondary")) {
            } else {
                temp.click();
            }
        }
    }

    public static void waitForPageToLoad(String url) {



        WebDriverWait wait = new WebDriverWait(webDriver, 40);
        wait.until(ExpectedConditions.urlContains(url));
        wait = new WebDriverWait(webDriver, 40);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax-loading")));
    }

    public static void minimiseChatWindow() {
        List<WebElement> zopimDivs = webDriver.findElements(By.className("zopim"));
        for (WebElement e : zopimDivs){
            if(e.getAttribute("style").contains("display: block")) {
                e.click(); //so e is the displayed element
                webDriver.switchTo().frame(e.findElement(By.tagName("iframe")));
                //splitdriver.findElement(By.className("overlay.jx_ui_Widget")).click();
                webDriver.findElement(By.className("meshim_widget_widgets_titleBar_MinimizeButton")).click();
                //zopimminimisebutton.click();
                // splitdriver.switchTo().defaultContent();
                webDriver.switchTo().parentFrame();
            }
        }
    }

    public static void clickItemInMultiPageList(List<WebElement> currentList, WebElement listBoxDiv, WebElement dropDownButton, String searchString) {
        //CLICKS ON A RANDOM ITEM IN A MULTI PAGE LIST
        //make it so it can be random, or specific.
        if (searchString.equalsIgnoreCase("random")) {
            int numItemsInAddressList = currentList.size();//gets number of items in list
            Random num = new Random();
            int randomNumberInList = 0;
            do {
                randomNumberInList = num.nextInt(numItemsInAddressList);//creates random number in range of number of items in list
            }
            while (randomNumberInList == 0);
            searchString = currentList.get(randomNumberInList).getText(); //the address we are looking for
        }
        //move mouse to be over first item in list
        //ON CHROME IT'S MOUSEDOWN+PAGEDOWN TO MOVE THROUGH THE BOX
        //scroll through list to find it (because we can't select things out of view)

        System.out.println("SEARCH STRING: " + searchString);

        while(
                ((currentList.get(currentList.indexOf((webDriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))).getLocation().getY()
                )+30) >
                        (
                                Math.round(FloatingDecimal.parseDouble(listBoxDiv.getCssValue("height").replace("px","")))+listBoxDiv.getLocation().getY()
                        )) {

            Actions action = new Actions(webDriver);

            action.moveToElement(listBoxDiv,30,30).clickAndHold().sendKeys(Keys.ARROW_DOWN).release().build().perform();

            waitForElementClickable(dropDownButton);
            if(!(listBoxDiv.isDisplayed())) {
                dropDownButton.click();
            }

        }
        currentList.get(currentList.indexOf((webDriver.findElement(By.xpath("//li/label[contains(text(),'"+ searchString +"')]"))).findElement(By.xpath("..")))).click();
    }


    public static void scrollToContinueButtonAndPress(WebElement btnContinue) {
        waitForPageToLoad("");

        //slowing down the process a bit to see if this avoids the error message at the end

        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 2); //use 300
            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("submit")));
        }catch (Exception e) {

        }
         //

        Integer a=1;
        do {
            try {
                btnContinue.click();
                a=1;
            } catch (Exception e) {
                a=(a+5);
                scrollToTop();
                JavascriptExecutor je = ((JavascriptExecutor) webDriver);
                je.executeScript("window.scrollByLines("+a+");");//TODO can we un-animate this scrolling?
                //scroll page
            }
        } while (a != 1);
    }

}