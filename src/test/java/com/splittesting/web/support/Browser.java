package com.splittesting.web.support;

public enum Browser {
    FIREFOX,
    CHROME
}
